# LogicSimulator

![screenshot](./screenshot_1.jpg)
**프로그램 내 아이콘들이 불친절함을 감안하시어 사용/수정 바랍니다.**

## 제작자

- 김유준([github 링크](https://github.com/demetoir))
- 박성규([github 링크](https://github.com/psk7142))

## 설명

- 학부 과정에서 배우는 논리 회로 설계를 간단한 수준에서 직접 설계 및 실험 할 수 있는 시뮬레이터 입니다.
- Windows SDK 버전 업데이트로 빌드 후 실행 가능한 수준을 유지하고 있습니다.
- 코어 엔진 파트와 랜더링 프로그램 파트로 나누어서 설계하였습니다.
- 코어 엔진은 `C++(Visual C++)`, 랜더링 프로그램은 `MFC`로 제작하였습니다.


## 빌드 버전

`Windows SDK 10.0.17134.0`에서 동작 확인
